﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Digger
{
    //Напишите здесь классы Player, Terrain и другие.
    public class Terrain : ICreature
    {
        public string GetImageFileName()
        {
            return "Terrain.png";
        }
        public int GetDrawingPriority()
        {
            return 3;
        }
        public bool DeadInConflict(ICreature conflictedObject)
        {
            return true;
        }
        public CreatureCommand Act(int x, int y)
        {
            return new CreatureCommand();
        }
    }

    public class Player : ICreature
    {
        public CreatureCommand Act(int x, int y)
        {

            var igr = new CreatureCommand();
            var key = Game.KeyPressed;
            if (key == Keys.Right && x + 1 < Game.MapWidth)
            {
                igr.DeltaX = 1;
            }
            else if (key == Keys.Left && x - 1 >= 0)
            {
                igr.DeltaX = -1;
            }
            else if (key == Keys.Up && y - 1 >= 0)
            {
                igr.DeltaY = -1;
            }
            else if (key == Keys.Down && y + 1 < Game.MapHeight)
            {
                igr.DeltaY = 1;
            }

            if (Game.Map[x + igr.DeltaX, y + igr.DeltaY] is Sack)
            {
                igr.DeltaX = 0;
                igr.DeltaY = 0;
            }
            if (Game.Map[x + igr.DeltaX, y + igr.DeltaY] is Sack)
            {
                igr.DeltaY = 0;
                igr.DeltaX = 0;
            }
            return igr;
        }

        public string GetImageFileName()
        {
            return "Digger.png";
        }
        public int GetDrawingPriority()
        {
            return 3;
        }
        public bool DeadInConflict(ICreature conflictedObject)
        {
            if (conflictedObject is Monster)
            {
                return conflictedObject is Monster;
            }
            return conflictedObject is Sack;
        }

    }

    public class Sack : ICreature
    {
        public string GetImageFileName()
        {
            return "Sack.png";
        }
       
        public int GetDrawingPriority()
        {
            return 4;
        }

        public int countSteps = 0;
        public CreatureCommand Act(int x, int y)
        {
            
            if (y+1 < Game.MapHeight)
            {
                if (countSteps > 0 && Game.Map[x, y+1] is Player
                    || countSteps > 0 && Game.Map[x, y+1] is Monster
                    || Game.Map[x, y + 1] == null)
                {
                    countSteps++;
                    return new CreatureCommand { DeltaX = 0, DeltaY = 1 };
                }
            }
            if (countSteps > 1 || y == Game.MapHeight-1)
            {
                return new CreatureCommand { DeltaX = 0, DeltaY = 0, TransformTo = new Gold() };
            }
            else
                countSteps = 0;

            return new CreatureCommand { DeltaX = 0, DeltaY = 0 };
        }
        public bool DeadInConflict(ICreature conflictedObject)
        {
            return false;
        }
    }

    public class Gold : ICreature
    {
        public string GetImageFileName()
        {
            return "Gold.png";
        }
        public int GetDrawingPriority()
        {
            return 3;
        }
        public CreatureCommand Act(int x, int y)
        {
            return new CreatureCommand();
        }
        public bool DeadInConflict(ICreature conflictedObject)
        {
            if (conflictedObject is Player)
            {
                Game.Scores += 10;
            }
            return true;
        }
    }
    public class Monster : ICreature
    {
        public bool PlayerOnMap()
        {
            for (var i = 0; i < Game.MapWidth; i++)
            {
                for (var j = 0; j < Game.MapHeight; j++)
                {
                    if (Game.Map[i, j] is Player)
                        return true;
                }
            }
            return false;
        }

        public int[] CoordinatesPlayer()
        {
            for (var i = 0; i < Game.MapWidth; i++)
            {
                for (var j = 0; j < Game.MapHeight; j++)
                {
                    if (Game.Map[i, j] is Player)
                        return new[] { i, j };
                }
            }
            return new int[0];
        }

        public bool GetNameObject(int x, int y)
        {
            return Game.Map[x, y] is Sack || Game.Map[x, y] is Monster || Game.Map[x, y] is Terrain;
        }

        public string GetImageFileName()
        {
            return "Monster.png";
        }
        public int GetDrawingPriority()
        {
            return 2;
        }

        public CreatureCommand Act(int x, int y)
        {
            var monster = new CreatureCommand { };

            if (PlayerOnMap())
            {
                if (CoordinatesPlayer()[0] < x)
                {
                    monster.DeltaX = -1;
                    if(!GetNameObject(x + monster.DeltaX, y + monster.DeltaY))
                        return monster;
                    {
                        monster.DeltaX = 0;
                        return monster;
                    }
                }

                if (CoordinatesPlayer()[0] > x)
                {
                    monster.DeltaX = 1;
                    if (!GetNameObject(x + monster.DeltaX, y + monster.DeltaY))
                        return monster;
                    {
                        monster.DeltaX = 0;
                        return monster;
                    }
                }

                if (CoordinatesPlayer()[1] < y)
                {
                    monster.DeltaY = -1;
                    if (!GetNameObject(x + monster.DeltaX, y + monster.DeltaY))
                        return monster;
                    {
                        monster.DeltaY = 0;
                        return monster;
                    }
                }

                if (CoordinatesPlayer()[1] > y)
                {
                    monster.DeltaY = 1;
                    if (!GetNameObject(x + monster.DeltaX, y + monster.DeltaY))
                        return monster;
                    {
                        monster.DeltaY = 0;
                        return monster;
                    }
                }
            }
            return monster;
        }
        public bool DeadInConflict(ICreature conflictedObject)
        {

            if (conflictedObject is Gold)
            { return conflictedObject is Monster; }

            return conflictedObject is Player ? false : true;
        }
    }

}
