﻿using System;
using System.Linq;

namespace Names
{
    internal static class HistogramTask
    {
public static HistogramData GetBirthsPerDayHistogram(NameData[] names, string name)
        {
            string[] months = new string[] {"1","2","3","4","5","6","7","8","9","10","11",
                    "12","13","14","15","16","17","18","19","20","21",
                    "22","23","24","25","26","27","28","29","30","31" };
            
            var birthsCounts = new double[months.Length];
            for (int i = 1; i < birthsCounts.Length; i++)
                foreach (var item in names)
                    if (item.BirthDate.Day == i + 1 && item.Name == name) birthsCounts[i]++;

            return new HistogramData(
                string.Format("Рождаемость людей с именем '{0}'", name), 
                months, 
                birthsCounts);
        }
    }
}