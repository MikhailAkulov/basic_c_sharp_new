﻿using NUnit.Framework.Internal.Execution;
using System.Collections.Generic;
using System.Linq;

namespace TextAnalysis
{
    static class FrequencyAnalysisTask
    {
        /*
         В этой задаче важно придумать декомпозицию всей задачи на 3-4 подзадачи.
        Если не получается самостоятельно, то в следующих подсказках предложены несколько подзадач, которые можно выделить из всей задачи.
        Одна из возможных подзадач — построить частотный словарь: начало N-граммы → последнее слово N-граммы → количество повторов этой N-граммы
        Вторая подзадача — по началу N-граммы выбирать самое частное продолжение.
        И, наконец, третья подзадача — собрать результаты остальных подзадач в решение всей задачи.
         */

        public static Dictionary<string, Dictionary<string, int>> BaseBigram(List<List<string>> text)
        {
            Dictionary<string, Dictionary<string, int>> bigram = new Dictionary<string, Dictionary<string, int>>();

            foreach (List<string> sent in text)
            {
                for (int i = 0; i < sent.Count-1; i++)
                {
                    if (!bigram.ContainsKey(sent[i]))
                        bigram.Add(sent[i], new Dictionary<string, int>());

                    if (!bigram[sent[i]].ContainsKey(sent[i + 1]))
                        bigram[sent[i]].Add(sent[i + 1], 1);
                    else bigram[sent[i]][sent[i + 1]] += 1;
                }
            }
            return bigram;
        }
        public static Dictionary<string, Dictionary<string, int>> BaseThrigram(List<List<string>> text)
        {
            Dictionary<string, Dictionary<string, int>> thrigram = new Dictionary<string, Dictionary<string, int>>();

            foreach (List<string> sent in text)
            {
                for (int i = 0; i < sent.Count - 2; i++)
                {
                    string bi = sent[i] + " " + sent[i + 1];
                    string bi2 = sent[i + 2];

                    if (!thrigram.ContainsKey(bi))
                        thrigram.Add(bi, new Dictionary<string, int>());

                    if (!thrigram[bi].ContainsKey(bi2))
                        thrigram[bi].Add(bi2, 1);
                    else thrigram[bi][bi2] += 1;
                }
            }
            return thrigram;
        }

        public static string GetMostFreqN(Dictionary<string, int> contNGram)
        {
            /*IComparer<KeyValuePair<string, int>> comp = Comparer<KeyValuePair<string, int>>.Create((x, y) =>
                {
                    int c1 = x.Value.CompareTo(y.Value);
                    if (c1 != 0) return c1;
                    return string.CompareOrdinal(x.Key, y.Key);

                });
            return contBiGram.Max(comp).Key;*/

            int maxNextWordFreq = int.MinValue;
            string mostFrequentNextWord = "";

            foreach (var nextWord in contNGram)
            {
                var nextWordFreq = nextWord.Value;
                if (maxNextWordFreq < nextWordFreq)
                {
                    maxNextWordFreq = nextWordFreq;
                    mostFrequentNextWord = nextWord.Key;
                }
                else if (maxNextWordFreq == nextWordFreq)
                        mostFrequentNextWord = GetLexicographicallyFirstWord(mostFrequentNextWord,
                                                                             nextWord.Key);
            }

            return mostFrequentNextWord;
            
        }
        public static string GetLexicographicallyFirstWord(string word1, string word2)
        {
            return string.CompareOrdinal(word1, word2) < 0 ? word1 : word2;
        }

        public static Dictionary<string, string> GetMostFrequentNextWords(List<List<string>> text)
        {
            var result = new Dictionary<string, string>();

            Dictionary<string, Dictionary<string, int>> rawBigrams = BaseBigram(text);
            Dictionary<string, Dictionary<string, int>> rawTrigrams = BaseThrigram(text);
            

            foreach (var word in rawBigrams)
            {
                result.Add(word.Key, GetMostFreqN(word.Value));
            }

            foreach (var word in rawTrigrams)
            {
                result.Add(word.Key, GetMostFreqN(word.Value));
            }

            return result;
        }

    }
}