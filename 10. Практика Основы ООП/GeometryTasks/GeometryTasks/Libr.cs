﻿using System;

namespace GeometryTasks
{
    //Vector.GetLength(), Segment.GetLength(), Vector.Add(Vector), Vector.Belongs(Segment) и Segment.Contains(Vector)
    public class Vector
    {
        public double X;
        public double Y;
        public double GetLength()
        {
            return Geometry.GetLength(this);
        }
        public Vector Add(Vector vector)
        {
            return Geometry.Add(this, vector);
        }
        public bool Belongs(Segment segment)
        {
            return Geometry.IsVectorInSegment(this, segment);
        }
    }

    public class Segment
    {
        public Vector Begin;
        public Vector End;
        public double GetLength()
        {
            return Geometry.GetLength(this);
        }
        public bool Contains(Vector vector)
        {
            return Geometry.IsVectorInSegment(vector, this);
        }
    }

    //Vector.GetLength(), Segment.GetLength(), Vector.Add(Vector), Vector.Belongs(Segment) и Segment.Contains(Vector)
    public class Geometry
    {
        public static double GetLength(Vector vector)
        {
            return Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y));
        }

        public static Vector Add(Vector vector1, Vector vector2)
        {
            return new Vector { X = vector1.X + vector2.X, Y = vector1.Y + vector2.Y };
        }

        public static double GetLength(Segment segment)
        {
            return Math.Sqrt(Math.Pow(segment.End.X -
                                        segment.Begin.X, 2) +
                                Math.Pow(segment.End.Y -
                                        segment.Begin.Y, 2));
        }
        public static bool IsVectorInSegment(Vector vector, Segment segment)
        {
            if ((vector.X == segment.Begin.X || vector.X == segment.End.X)
                && (vector.Y == segment.End.Y || vector.Y == segment.Begin.Y))
            {
                return true;
            }
            else
            {
                return ((vector.X - segment.Begin.X) * (vector.X - segment.End.X) <= 0)
                    && ((vector.Y - segment.Begin.Y) * (vector.Y - segment.End.Y) < 0);
            }
        }
    }
}


