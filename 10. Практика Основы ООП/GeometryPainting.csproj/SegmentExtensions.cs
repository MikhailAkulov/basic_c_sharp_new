﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeometryTasks;
using System.Drawing;

namespace GeometryPainting
{
    //Напишите здесь код, который заставит работать методы segment.GetColor и segment.SetColor
    public static class SegmentExtentions
    {
        public static Dictionary<Segment, Color> colors = new Dictionary<Segment, Color>();

        public static void SetColor(this Segment segment, Color color)
        {
            if (colors.Count > 0 && color != null)
            {
                if (colors.ContainsKey(segment))
                    colors.Remove(segment);
                colors.Add(segment, color);

            }
            else if (color != null) colors.Add(segment, color);
        }
        public static Color GetColor(this Segment segment)
        {
            if (segment != null)
            {
                Color val;
                if (colors.TryGetValue(segment, out val)) return colors[segment];
            }
            return Color.Black;
        }
    }
}
