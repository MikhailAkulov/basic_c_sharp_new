﻿namespace Mazes
{
	public static class SnakeMazeTask
	{
		public static void MoveOut(Robot robot, int width, int height)
		{
			while (!robot.Finished)
			{
				MoveRightMax(robot, width);
				MoveDownMax(robot);
				MoveLeftMax(robot, width);
				if (!robot.Finished)
				{
					MoveDownMax(robot);
				}
			}
		}

		private static void MoveRightMax(Robot robot, int width)
		{
			for (int i = 0; i < width - 3; i++)	robot.MoveTo(Direction.Right);
		}

		private static void MoveLeftMax(Robot robot, int width)
		{
			for (int i = 0; i < width - 3; i++) robot.MoveTo(Direction.Left);
		}

		private static void MoveDownMax(Robot robot)
		{
			robot.MoveTo(Direction.Down);
			robot.MoveTo(Direction.Down);
		}
	}
}